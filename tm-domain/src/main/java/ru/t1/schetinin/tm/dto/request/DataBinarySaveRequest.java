package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable final String token) {
        super(token);
    }

}