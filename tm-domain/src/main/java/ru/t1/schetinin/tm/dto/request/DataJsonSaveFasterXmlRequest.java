package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}