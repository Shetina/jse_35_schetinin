package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataYamlLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}