package ru.t1.schetinin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable final String token) {
        super(token);
    }

}
