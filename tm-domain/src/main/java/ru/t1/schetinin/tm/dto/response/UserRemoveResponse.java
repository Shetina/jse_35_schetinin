package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@NotNull final User user) {
        super(user);
    }

}