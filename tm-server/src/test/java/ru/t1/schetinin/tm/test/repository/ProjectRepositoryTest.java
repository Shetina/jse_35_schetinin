package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project number " + i);
            project.setDescription("Description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        PROJECT_REPOSITORY.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        final int expectedNumberOfEntriesUser = PROJECT_REPOSITORY.getSize(USER_ID_1) + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final Project project = new Project();
        PROJECT_REPOSITORY.add(project);
        PROJECT_REPOSITORY.add(userId, project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(expectedNumberOfEntriesUser, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.add(userId,null));
    }

    @Test
    public void testExistById() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(project.getId()));
        Assert.assertTrue(PROJECT_REPOSITORY.existsById(project.getUserId(), project.getId()));
    }

    @Test
    public void testCreateProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String projectName = "project-test-1";
        @NotNull final Project project = PROJECT_REPOSITORY.create(userId, projectName);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void testCreateProjectWithDesc() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = USER_ID_1;
        @NotNull final String projectName = "project-test-1";
        @NotNull final String description = "desc project 1";
        @NotNull final Project project = PROJECT_REPOSITORY.create(userId, projectName, description);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(userId, project.getUserId());
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull final List<Project> projectsNew = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project number" + i * 2);
            project.setDescription("Description " + i * 2);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectsNew.add(project);
        }
        PROJECT_REPOSITORY.add(projectsNew);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = PROJECT_REPOSITORY.findAll();
        @NotNull final List<Project> projects2 = PROJECT_REPOSITORY.findAll(USER_ID_1);
        Assert.assertEquals(projects.size(), PROJECT_REPOSITORY.getSize());
        Assert.assertEquals(projects2.size(), PROJECT_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testFindAllSort() {
        @NotNull final String sortType = "BY_NAME";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Project> projectSort = PROJECT_REPOSITORY.findAll(USER_ID_1, sort.getComparator());
        Assert.assertNotNull(PROJECT_REPOSITORY.findAll(sort.getComparator()));
        Assert.assertEquals(PROJECT_REPOSITORY.findAll(USER_ID_1), projectSort);
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<Project> projects = PROJECT_REPOSITORY.findAll(USER_ID_1);
        @NotNull final Project project1 = projects.get(0);
        @NotNull final String projectId = project1.getId();
        Assert.assertEquals(project1, PROJECT_REPOSITORY.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final Project project1 = PROJECT_LIST.get(0);
        @NotNull final Project project2 = PROJECT_LIST.get(PROJECT_REPOSITORY.getSize() - 1);
        @NotNull final Project project3 = PROJECT_LIST.get(PROJECT_REPOSITORY.getSize() / 2);
        Assert.assertEquals(project1, PROJECT_REPOSITORY.findOneByIndex(0));
        Assert.assertEquals(project2, PROJECT_REPOSITORY.findOneByIndex(PROJECT_REPOSITORY.getSize() - 1));
        Assert.assertEquals(project3, PROJECT_REPOSITORY.findOneByIndex(PROJECT_REPOSITORY.getSize() / 2));
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.remove(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.remove(USER_ID_1, null));
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.removeById(project.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIdUserNegative() {
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertNull(PROJECT_REPOSITORY.removeById(USER_ID_1, null));
        Assert.assertNull(PROJECT_REPOSITORY.removeById(USER_ID_FAKE, project.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        PROJECT_REPOSITORY.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndexUser() {
        final int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(USER_ID_1) - 1;
        PROJECT_REPOSITORY.removeByIndex(USER_ID_1, 0);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(USER_ID_1));
        Assert.assertNull(PROJECT_REPOSITORY.removeByIndex(USER_ID_FAKE, 1));
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.removeAll(PROJECT_LIST);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testSet() {
        @NotNull List<Project> projects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projects.add(new Project());
        }
        PROJECT_REPOSITORY.set(projects);
        Assert.assertEquals(projects, PROJECT_REPOSITORY.findAll());
    }

}
