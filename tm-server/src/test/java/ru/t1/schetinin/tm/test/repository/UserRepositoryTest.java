package ru.t1.schetinin.tm.test.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final List<User> USER_LIST = new ArrayList<>();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("LOGIN " + i);
            @Nullable final String hash = HashUtil.salt(PROPERTY_SERVICE, "password" + i);
            Assert.assertNotNull(hash);
            user.setPasswordHash(hash);
            user.setEmail("user" + i + "@mail.ru");
            USER_REPOSITORY.add(user);
            USER_LIST.add(user);
        }
    }

    @After
    public void clearData() {
        USER_REPOSITORY.clear();
        USER_LIST.clear();
    }

    @Test
    public void testFindByLogin() {
        @NotNull final User user1 = USER_LIST.get(0);
        @Nullable final User user2 = USER_REPOSITORY.findByLogin(user1.getLogin());
        Assert.assertEquals(user1, user2);
        Assert.assertNull(USER_REPOSITORY.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        @NotNull final User user1 = USER_LIST.get(0);
        @Nullable final User user2 = USER_REPOSITORY.findByEmail(user1.getEmail());
        Assert.assertEquals(user1, user2);
        Assert.assertNull(USER_REPOSITORY.findByEmail(null));
    }

    @Test
    public void testIsLoginExist() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertTrue(USER_REPOSITORY.isLoginExist(user.getLogin()));
    }

    @Test
    public void testIsEmailExist() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertTrue(USER_REPOSITORY.isEmailExist(user.getEmail()));
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User user = new User();
        USER_REPOSITORY.add(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            users.add(new User());
        }
        USER_REPOSITORY.add(users);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        USER_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> users = USER_REPOSITORY.findAll();
        Assert.assertEquals(users.size(), USER_REPOSITORY.getSize());
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<User> users = USER_REPOSITORY.findAll();
        @NotNull final User user1 = users.get(0);
        @NotNull final String projectId = user1.getId();
        Assert.assertEquals(user1, USER_REPOSITORY.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final User user1 = USER_LIST.get(0);
        @NotNull final User user2 = USER_LIST.get(USER_REPOSITORY.getSize() - 1);
        @NotNull final User user3 = USER_LIST.get(USER_REPOSITORY.getSize() / 2);
        Assert.assertEquals(user1, USER_REPOSITORY.findOneByIndex(0));
        Assert.assertEquals(user2, USER_REPOSITORY.findOneByIndex(USER_REPOSITORY.getSize() - 1));
        Assert.assertEquals(user3, USER_REPOSITORY.findOneByIndex(USER_REPOSITORY.getSize() / 2));
    }

    @Test
    public void testSet() {
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            users.add(new User());
        }
        USER_REPOSITORY.set(users);
        Assert.assertEquals(users, USER_REPOSITORY.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_REPOSITORY.remove(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_REPOSITORY.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        USER_REPOSITORY.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        USER_REPOSITORY.removeAll(USER_LIST);
        Assert.assertEquals(expectedNumberOfEntries, USER_REPOSITORY.getSize());
    }

}