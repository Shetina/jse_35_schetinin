package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.field.*;
import ru.t1.schetinin.tm.exception.user.UserNotFoundException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.service.UserService;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final IProjectRepository PROJECT_REPOSITORY = new ProjectRepository();

    @NotNull
    private static final ITaskRepository TASK_REPOSITORY = new TaskRepository();

    @NotNull
    private static final List<User> USER_LIST = new ArrayList<>();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(PROPERTY_SERVICE, USER_REPOSITORY, PROJECT_REPOSITORY, TASK_REPOSITORY);

    @NotNull
    private static final String STRING_FAKE = "THISISFAKE";

    @NotNull
    private static final String ID_FAKE = UUID.randomUUID().toString();

    @Before
    public void initDemoData() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("LOGIN " + i);
            @Nullable final String hash = HashUtil.salt(PROPERTY_SERVICE, "password" + i);
            Assert.assertNotNull(hash);
            user.setPasswordHash(hash);
            user.setEmail("user" + i + "@mail.ru");
            USER_SERVICE.add(user);
            USER_LIST.add(user);
        }
    }

    @After
    public void clearData() {
        USER_SERVICE.clear();
        USER_LIST.clear();
    }

    @Test
    public void testFindByLogin() {
        @NotNull final User user1 = USER_LIST.get(0);
        @Nullable final User user2 = USER_SERVICE.findByLogin(user1.getLogin());
        Assert.assertEquals(user1, user2);
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        @NotNull final User user1 = USER_LIST.get(0);
        @Nullable final User user2 = USER_SERVICE.findByEmail(user1.getEmail());
        Assert.assertEquals(user1, user2);
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(null));
    }

    @Test
    public void testIsLoginExist() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertTrue(USER_SERVICE.isLoginExist(user.getLogin()));
    }

    @Test
    public void testIsEmailExist() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertTrue(USER_SERVICE.isEmailExist(user.getEmail()));
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User user = new User();
        USER_SERVICE.add(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            users.add(new User());
        }
        USER_SERVICE.add(users);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        USER_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> users = USER_SERVICE.findAll();
        Assert.assertEquals(users.size(), USER_SERVICE.getSize());
    }

    @Test
    public void testFindOneById() {
        @NotNull final List<User> users = USER_SERVICE.findAll();
        @NotNull final User user1 = users.get(0);
        @NotNull final String projectId = user1.getId();
        Assert.assertEquals(user1, USER_SERVICE.findOneById(projectId));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final User user1 = USER_LIST.get(0);
        @NotNull final User user2 = USER_LIST.get(USER_SERVICE.getSize() - 1);
        @NotNull final User user3 = USER_LIST.get(USER_SERVICE.getSize() / 2);
        Assert.assertEquals(user1, USER_SERVICE.findOneByIndex(0));
        Assert.assertEquals(user2, USER_SERVICE.findOneByIndex(USER_SERVICE.getSize() - 1));
        Assert.assertEquals(user3, USER_SERVICE.findOneByIndex(USER_SERVICE.getSize() / 2));
    }

    @Test
    public void testSet() {
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            users.add(new User());
        }
        USER_SERVICE.set(users);
        Assert.assertEquals(users, USER_SERVICE.findAll());
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_SERVICE.remove(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_SERVICE.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        USER_SERVICE.removeByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        USER_SERVICE.removeAll(USER_LIST);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testCreateUser() {
        final int expectedNumberOfUsers = NUMBER_OF_ENTRIES + 3;
        @NotNull final User user1 = USER_SERVICE.create("login1", "password1");
        @NotNull final User user2 = USER_SERVICE.create("login2", "password2", "email2@mail.ru");
        @NotNull final User user3 = USER_SERVICE.create("login3", "password3", Role.ADMIN);
        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
        Assert.assertEquals("login1", user1.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "password1"), user1.getPasswordHash());
        Assert.assertEquals("email2@mail.ru", user2.getEmail());
        Assert.assertEquals(Role.ADMIN, user3.getRole());
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, STRING_FAKE, STRING_FAKE));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(STRING_FAKE, null, STRING_FAKE));
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertThrows(ExistsLoginException.class, () -> USER_SERVICE.create(user.getLogin(), STRING_FAKE, STRING_FAKE));
        Assert.assertThrows(ExistsEmailException.class, () -> USER_SERVICE.create(STRING_FAKE, ID_FAKE, user.getEmail()));
    }

    @Test
    public void testRemoveByLogin() {
        final int expectedNumberOfUsers = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.removeByLogin(user.getLogin()));
        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
    }


    @Test
    public void testRemoveByEmail() {
        final int expectedNumberOfUsers = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.removeByEmail(user.getEmail()));
        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(null));
    }

    @Test
    public void testRemoveOne() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = USER_LIST.get(0);
        USER_SERVICE.removeOne(user);
        Assert.assertEquals(expectedNumberOfEntries, USER_SERVICE.getSize());
    }

    @Test
    public void testSetPassword() {
        @NotNull final User user = USER_LIST.get(0);
        @NotNull final String password = "password0000";
        @Nullable final String passHash = HashUtil.salt(PROPERTY_SERVICE, password);
        Assert.assertNotNull(USER_SERVICE.setPassword(user.getId(), password));
        Assert.assertEquals(passHash, user.getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, STRING_FAKE));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(STRING_FAKE, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(STRING_FAKE, STRING_FAKE));
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final User user = USER_LIST.get(0);
        Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
        Assert.assertEquals(true, user.getLocked());
        Assert.assertNotNull(USER_SERVICE.unlockUserByLogin(user.getLogin()));
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
    }

    @Test
    public void testUpdateUser() {
        @NotNull final User user = USER_LIST.get(0);
        @NotNull final String FIO = "FIO";
        Assert.assertNotNull(USER_SERVICE.updateUser(user.getId(), FIO, FIO, FIO));
        Assert.assertEquals(FIO, user.getFirstName());
        Assert.assertEquals(FIO, user.getMiddleName());
        Assert.assertEquals(FIO, user.getLastName());
    }

    @Test
    public void testUpdateUserNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(null, STRING_FAKE, STRING_FAKE, STRING_FAKE));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateUser(STRING_FAKE, STRING_FAKE, STRING_FAKE, STRING_FAKE));
    }

}